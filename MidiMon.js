/*************************************************************************
 *
 * Script:  MidiMon
 *
 * Version: 1.0.0
 *
 * Scripter based Midi Monitor with many options in the GUI
 *
 *************************************************************************/

var NeedsTimingInfo = true;


/*****************************
 * HandleMIDI
 *****************************/

function HandleMIDI(event) {
    
    // first forward on the event if thru enabled
    if (guiThru()==true) {
        event.send();
    }
 
    // check monitoring mode
    if(monitoring() == false) {
        return;
    }

    // potentially log the event
    event.midimon();
}

/************************************
 * check monitor mode and compare to
 * playing state to determine whether
 * to log or not
 ************************************/

var gTempo = 0;

function monitoring() {

    let mode = guiMonMode();

    if( mode == MONOFF ) {
        return false;
    }

    if(mode==MONPLAY) {
        let info=GetTimingInfo();
        gTempo=info.tempo;
        if(info.playing) {
            return true;
        }
        else {
            return false;
        }
    }

    if(mode==MONALWAYS) {
        if(guiShowTiming() > 0) {
            let info=GetTimingInfo();
            gTempo=info.tempo;
        }
        return true;
    }
}


/*****************************
 * Event.midimon()
 *****************************/

Event.prototype.midimon = function() {
    if(this.guiShowEvent() == true) {
        this.enqueueLog();
    }
};

/***********************************
 * Per event formatting
 ***********************************/

Event.prototype.format = function() {
    return ("Unknown Event type");
};

NoteOn.prototype.format = function() {
    let json = {};
    json.status = "NoteOn".padEnd(16),
    json.data1 =  this.pitchStr();
    json.data2 =  this.velocityStr();
    json.notes = ""
    return this.formatMidimon(json);
};

NoteOff.prototype.format = function() {
    let json = {};
    json.status = "NoteOff".padEnd(16);
    json.data1 = this.pitchStr();
    json.data2 = this.velocityStr();
    json.notes = "";
    return this.formatMidimon(json);
};

ControlChange.prototype.format = function() {
    let json = {};
    json.status = "ControlChange".padEnd(16);
    json.data1 = this.numberStr();
    json.data2 = this.valueStr();
    json.notes = "(" + MIDI.ccName(this.number) + ")";
   return this.formatMidimon(json);
};


ProgramChange.prototype.format = function() {
    let json = {};
    json.status = "ProgramChange".padEnd(16);
    json.data1 = this.numberStr();
    json.data2 = "             ";
    json.notes = "";
   return this.formatMidimon(json);
};

PitchBend.prototype.format = function() {
    let json = {};
    json.status = "PitchBend".padEnd(16);
    json.data1 = "       ";
    json.data2 = this.valueStr();
    json.notes = "";
   return this.formatMidimon(json);
};

ChannelPressure.prototype.format = function() {
    let json = {};
    json.status = "ChannelPressure".padEnd(16);
    json.data1 = "       ";
    json.data2 = this.valueStr();
    json.notes = "";
   return this.formatMidimon(json);
};

PolyPressure.prototype.format = function() {
    let json = {};
    json.status = "PolyPressure".padEnd(16);
    json.data1 = this.pitchStr();
    json.data2 = this.valueStr();
    json.notes = ""
   return this.formatMidimon(json);
};

/******************************************************************
 * format beatPos pos field
 *
 * TODO, can any of this be more efficient?
 ******************************************************************/

Event.prototype.timingStr = function() {

    let show = guiShowTiming();

    // if timing info turned off
    if (show == 0) {
        return ("");
    }

    // for fractional beat type
    if (show == 2) {

        var base = Math.trunc(this.beatPos);
        var baseOut = base.toString().padStart(4);
        var frac = Math.round((this.beatPos - base) * (10000000000));
        var fracOut = frac.toString().padEnd(10, "0");

        var out = "[" + baseOut + "." + fracOut + "] ";
        return out;
    }

    // for ms fraction beat type
    if (show == 3) {

        var base = Math.trunc(this.beatPos);
        var baseOut = base.toString().padStart(4);
        var frac = this.beatPos - base;

        var ms = Math.round(60000/gTempo * frac);  // convert frac to ms
        
        var fracOut = ms.toString().padStart(4, " ");
        var out = "[" + baseOut + ":" + fracOut + " ms] ";
        return out;
    }

    // otherwise convert to ticks

    var totalTicks = Math.round(this.beatPos * guiPPQN());
    var baseBeat = Math.floor(totalTicks / guiPPQN());
    var ticks = totalTicks % guiPPQN();

    var baseOut = baseBeat.toString().padStart(4);
    var ticksOut = ticks.toString().padStart(4);
    var out = "[" + baseOut + "|" + ticksOut + "] ";
    return out;
};

/******************************************************************
 * format pitch field
 ******************************************************************/

Event.prototype.pitchStr = function() {
    return (`[${MIDI.noteName(this.pitch)}]`).padEnd(7);
};

/******************************************************************
 * format velocity field
 ******************************************************************/

Note.prototype.velocityStr = function() {
    let name = this.velocity.toString().padStart(3);
    return `velocity:${name} `;
};

/******************************************************************
 * format number field
 ******************************************************************/

Event.prototype.numberStr = function() {
    return (`[#${this.number.toString()}]`).padEnd(7);
};

/******************************************************************
 * format value field
 ******************************************************************/

Event.prototype.valueStr = function() {
    let value = this.value.toString().padStart(6);
    return `value:${value} `;
};

/******************************************************************
 * format value field for PitchBend
 ******************************************************************/

PitchBend.prototype.valueStr = function() {
    let value = this.value.toString().padStart(6);
    return `value:${value} `;
};


/******************************************************************
 * format port field
 ******************************************************************/

Event.prototype.portStr = function() {
    if (guiShowPort() > 0) {
        var port;
        if(this.port == undefined || this.port==0) port = "--";
        else port = (this.port).toString();
        return ("port:" + port.padStart(2, " ") + " ");
    }
    else {
        return "";
    }
};

/******************************************************************
 * format channel field
 ******************************************************************/

Event.prototype.channelStr = function() {
    if (guiShowChannel()) {
        return ("ch:" + this.channel.toString().padStart(2, " ") + " ");
    }
    else {
        return "";
    }
};

/******************************************************************
 * format bytes field
 ******************************************************************/

Event.prototype.byteStr = function() {
    if (guiShowBytes()) {
        return ("bytes:" + this.status.toString(16) + " " +
              this.data1.toString(16) + " " +
              this.data2.toString(16) + " ");
    }
    else {
        return "";
    }
};

/******************************************************************
 * format articulationIDS field
 ******************************************************************/

Event.prototype.artIdStr = function() {
    let showid = guiShowArtId();
    if (showid==ARTALWAYS 
            || (showid==ARTNONZERO && this.articulationID > 0)) {
        var out = this.articulationID.toString().padEnd(3);
        return ("art:" + out + " ");
    }
    else {
        return "";
    }
};

/******************************************************************
 * Format the whole Event line for output
 ******************************************************************/

Event.prototype.formatMidimon = function(json) {
    return(   this.timingStr() 
            + json.status
            + this.channelStr()
            + this.portStr()
            + json.data1
            + json.data2
            + this.byteStr()
            + this.artIdStr()
            + json.notes);
};

/*******************************************
 * Buffered Logging
 *******************************************/

// flush the queue - best called from Idle()
function flush() {
    var i = 0;
    let mf = guiMaxFlush();
    while (midimonQueue.length > 0 && i < mf) {
        var event = midimonQueue.shift();
        Trace(event.format());
        i++;
    }
}

function Idle() {
    // flush any logged midi events
    flush();
}

/**************************************************
 * Use OOP approach to block PrimingEvent from 
 * getting logged
 **************************************************/

var midimonQueue = [];

Event.prototype.enqueueLog = function() {
    // noop for parent class to block PrimingEvent
};
NoteOn.prototype.enqueueLog = function() {
    midimonQueue.push(this);
};
NoteOff.prototype.enqueueLog = function() {
    midimonQueue.push(this);
};
ControlChange.prototype.enqueueLog = function() {
    midimonQueue.push(this);
};
PitchBend.prototype.enqueueLog = function() {
    midimonQueue.push(this);
};
ProgramChange.prototype.enqueueLog = function() {
    midimonQueue.push(this);
};
ChannelPressure.prototype.enqueueLog = function() {
    midimonQueue.push(this);
};
PolyPressure.prototype.enqueueLog = function() {
    midimonQueue.push(this);
};

/*************************
 * some macros
 *************************/

const MONPLAY   = 0;
const MONALWAYS = 1;
const MONOFF    = 2;

const ARTOFF     = 0;
const ARTALWAYS   = 1;
const ARTNONZERO = 2;

var ppqnData = [];

/***************************
 * GUI
 ***************************/

PluginParameters = [];
  
  PluginParameters.push({
    name: "Monitoring",
    type: "menu",
    valueStrings: ["While Playing", "Always", "🚫 Disabled"],
    numberOfSteps: 3,
    defaultValue: 1
  });

  PluginParameters.push({
    name: "—————— Display ——————",
    type: "text"
  });

  PluginParameters.push({
    name: "Timing Info 🕒",
    type: "menu",
    valueStrings: ["🚫 Disabled", "As PPQN ticks", "As Fractional Beats", 
                   "Fraction As Milliseconds"],
    defaultValue: 1
  });
  
  PluginParameters.push({
    name: "PPQN",
    type: "menu",
    valueStrings: ["3840","3072","1920","1536","960","768","480",
                   "384","240","192","120","96","48","24"],
    defaultValue: 4
  });
  ppqnData = [3840,3072,1920,1536,960,768,480,384,240,192,120,96,48,24];
  
  PluginParameters.push({
    name: "Show Port",
    type: "checkbox",
    defaultValue: 0
});

  PluginParameters.push({
    name: "Show Channel",
    type: "checkbox",
    defaultValue: 1
});

PluginParameters.push({
    name: "Show Articulation ID",
    type: "menu",
    valueStrings:["🚫 Disabled","Always","Non-Zero"],
    defaultValue: 0
  });

  PluginParameters.push({
    name: "Show Bytes",
    type: "checkbox",
    defaultValue: 0
  });

  PluginParameters.push({
    name: "—————— Filter ——————",
    type: "text"
  });

  PluginParameters.push({
    name: "Note On",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "Note Off",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "Control Change",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "Program Change",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "Pitch Bend",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "Channel Pressure",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "Poly Pressure",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "—————— Options ——————",
    type: "text"
  });

  PluginParameters.push({
    name: "Thru→",
    type: "checkbox",
    defaultValue: 1
  });

  PluginParameters.push({
    name: "Display Middle-C as",
    type: "menu",
    valueStrings: ["Yamaha C3","Roland C4"],
    defaultValue: 0
  });

   PluginParameters.push({
     name: "Events per Flush",
     type: "lin",
     defaultValue: 20,
     minValue: 0,
     maxValue: 50,
     numberOfSteps: 50
   });


function guiMonMode() {
    return GuiParameter(0);
}

// 1 is text

function guiShowTiming() {
    return GuiParameter(2);
}

function guiPPQN() {
    return ppqnData[GuiParameter(3)];
}

function guiShowPort() {
    return GuiParameter(4);
}

function guiShowChannel() {
    if(GuiParameter(5)==0) return false;
    else return true;
}

function guiShowArtId() {
    return GuiParameter(6);
}

function guiShowBytes() {
    if(GuiParameter(7)==0) return false;
    else return true;
}

// 8 is text

NoteOn.prototype.guiShowEvent = function() {
    if(GuiParameter(9)==1) return true;
    else return false;
};

NoteOff.prototype.guiShowEvent = function() {
    if(GuiParameter(10)==1) return true;
    else return false;
};

ControlChange.prototype.guiShowEvent = function() {
    if(GuiParameter(11)==1) return true;
    else return false;
};

ProgramChange.prototype.guiShowEvent = function() {
    if(GuiParameter(12)==1) return true;
    else return false;
};

PitchBend.prototype.guiShowEvent = function() {
    if(GuiParameter(13)==1) return true;
    else return false;
};

ChannelPressure.prototype.guiShowEvent = function() {
    if(GuiParameter(14)==1) return true;
    else return false;
};

PolyPressure.prototype.guiShowEvent = function() {
    if(GuiParameter(15)==1) return true;
    else return false;
};

// 16 is text

function guiThru() {
    if(GuiParameter(17)==0) return false;
    else return true;
}

function guiMidC() {
    if(GuiParameter(18)==0) return 0;
    else return 12;
}

function guiMaxFlush() {
    return GuiParameter(19);
}



function ParameterChanged(id, val) {
    PluginParameters[id].data = val;
}

// Faster function to get GUI value
function GuiParameter(id) {

    // just in case programmer error
    if(id >= PluginParameters.length) return undefined;
    
    // if script was recently initialized, reload GUI value
    if(PluginParameters[id].data == undefined) {
        PluginParameters[id].data = GetParameter(id);
    }

    return PluginParameters[id].data;
}
