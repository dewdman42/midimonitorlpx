--[[
name: Midi Logger
description: Midi Logging converted from Scipter Javascript
author: Steve Schow
version: 1.0

Initially just use simple print, but later on need to find 
a way to do buffered logging, outside of the process block

TODO

     - Add basic GUI support to support various options
     - Buffered logging
     - Buffered logging on seperate thread
     - Try to see if I can make the script using polymorphic approach like javascript version
     - Does LUA even have objects and classes, easily?  Perhaps have to use tables somehow
     - Optionally log to a file in addition to console.
     - Optionally log in midicsv format?
     - Display time as Bars/Beats, or as bars/beats/PPQN if that is provided, for now
     - let's display as seconds with 3 fractional digits for miliseconds, but later on 
     - calculate bars, beats, ticks, etc.
     - Can we do the timestamp like LogicPro does...keep it running somehow even when 
       stopped?
     
     - Take a look at how the lua protoplug guy did his libraries for tips 
     - on how to structure my libraries into namespaces or classes or something like that  modules?
     - how to clear the console whenever I compile
     - what is the little prpmt at the bottom for?
     
     - The console window doesn't scroll very far back...so...probably need to use a log file option
     
     - Other useful functions I need to add:
      
          + table stringify (if it doesn't already exist)
          
          + 
          
     - QUESTIONS
     
          - Can I edit these files in vim and automatically load here somehow?
          - why doesn't io.write work?
     
--]]

require "include/protoplug"


local blockEvents = {}


function plugin.processBlock(samples, smax, midiBuf)
   
    local something=false
    
	for ev in midiBuf:eachEvent() do
	
	    something=true
	
	    info = plugin.getCurrentPosition()
       	table.insert(blockEvents, {beatPos=info.ppqPosition, 
       	                           event=midi.Event(ev)})
	    
	end
	
    -- if no midi events in this block, the flush the buffer
	-- TODO, set limit to how many can be flushed per process block
	if (something == false) then
	
	    for i = 1, #blockEvents do

			
			local be = blockEvents[i]
			
	        --Call function for each Event type to log i
	        --TODO, can we use polymorphism like in js?
	    
	        eventType = "Unknown Event Type"

	        local evt = be.event
	        local BBT = makeTimingStr(be.beatPos, evt)
	    
	        -------------------------
	        -- NoteOn
	        -------------------------	    
	    
	        if evt:isNoteOn() then		   
	    	    eventType = "NoteOn"	
		        data1 = NoteNames[evt:getNote()]
		        data2 = string.format("%3d",evt:getVel())
		   	    

		    elseif evt:isNoteOff() then
		        eventType = "NoteOff"
		        data1 = NoteNames[evt:getNote()]
		        data2 = string.format("%3d",evt:getVel())
		   
            elseif evt:isControl() then
                eventType = "ControlChange"
 		        data1 = string.format("#%-3d", evt:getControlNumber())
		        data2 = string.format("%3d",evt:getControlValue())
		             
	     	end	

	    	log(string.format("[%s %-13s ch:%-2d %-4s %3s", BBT, eventType, 
		                                        evt:getChannel(), data1, data2))			
						
		end
		blockEvents = {}
		
	end
end

SHOWTIMING = 1
PPQN = 960

-- TODO, this can be much simple with sprint conslidation
-- TODO also the math needs to also add the offset of the event
-- in the block and convert samples to whatever format is being 
-- output

function makeTimingStr(beatPos) 

    -- for fractional beat type
    
    if (SHOWTIMING == 2) then

        --base = math.trunc(beatPos);
        base = math.floor(beatPos)
        baseOut = string.format("%4d",base)
        frac = math.floor((beatPos - base + .5) * (10000000000))
        fracOut = string.format("%10d",frac)  -- need to pad with zeros

        out = "[" .. baseOut .. "." .. fracOut .. "] "
        
        return out;
    end

    -- for ms fraction beat type
    -- TODO ms is provided so use that or convert?
    if (show == 3) then

        base = math.floor(beatPos)
        baseOut = string.format("%4d", base)
        frac = beatPos - base

        ms = math.floor((60000/gTempo * frac)+.5)  -- convert frac to ms
        
        fracOut = format.string("%4d", ms)
        out = "[" .. baseOut .. ":" .. fracOut .. " ms] "
        return out
    end

    -- otherwise convert to ticks

    totalTicks = math.floor(beatPos * PPQN + .5)
    baseBeat = math.floor(totalTicks / PPQN)
    ticks = totalTicks % PPQN

    baseOut = string.format("%4d",baseBeat)
    ticksOut = string.format("%4d",ticks)
    out = "[" .. baseOut .. "|" .. ticksOut .. "] "
    return out

end



--------------------------------------------
-- sendEvents
-- function that replaces the provided
-- block buffer with events from given table
--------------------------------------------
function sendEvents(table, block)
    block:clear()
	if #table>0 then
		for _,e in ipairs(table) do
			block:addEvent(e)
		end
	end      
end

-----------------------------------------------
-- for now just send it, later on use buffering
-----------------------------------------------
function log(msg)
    print(msg)
end


-------------------------------------------------
-- Build up some global arrays to hold NoteNames
-- and numbers for conversion
-- TODO, move into a module and use function to 
-- access the value
-------------------------------------------------

NoteNames={}
NoteNumbers={}
rootNames = {"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"}

for i=0,127,1
do
    octaveName = string.format("%d",math.floor(i / 12) - 2)
    m = i % 12 + 1
    elem = rootNames[m] .. octaveName
    NoteNumbers[elem] = i
    NoteNames[i] = elem
end


                    





function print_table(node)
    -- to make output beautiful
    local function tab(amt)
        local str = ""
        for i=1,amt do
            str = str .. "\t"
        end
        return str
    end
 
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"
 
    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end
 
        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then
               
                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end
 
                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""
               
                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end
 
                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. tab(depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. tab(depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. tab(depth) .. key .. " = '"..tostring(v).."'"
                end
 
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. tab(depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. tab(depth-1) .. "}"
                end
            end
 
            cur_index = cur_index + 1
        end
 
        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end
 
    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)
   
    print(output_str)
end

