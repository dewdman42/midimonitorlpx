// Down here are functions that might end up in a namespace library
// for reuse between simple scripts.
// Could handle the rangeID better so taht its not neccessary to lookup
// Array element as many times...which is expensive in angelscript

#include "../../library/Midi.hxx"

namespace MyMidi
{
    dictionary NoteNumbers = {
        {"C-2",0},{"C#-2",1},{"D-2",2},{"D#-2",3},{"E-2",4},{"F-2",5},
        {"F#-2",6},{"G-2",7},{"G#-2",8},{"A-2",9},{"A#-2",10},{"B-2",11},
        {"C-1",12},{"C#-1",13},{"D-1",14},{"D#-1",15},{"E-1",16},{"F-1",17},
        {"F#-1",18},{"G-1",19},{"G#-1",20},{"A-1",21},{"A#-1",22},{"B-1",23},
        {"C0",24},{"C#0",25},{"D0",26},{"D#0",27},{"E0",28},{"F0",29},
        {"F#0",30},{"G0",31},{"G#0",32},{"A0",33},{"A#0",34},{"B0",35},
        {"C1",36},{"C#1",37},{"D1",38},{"D#1",39},{"E1",40},{"F1",41},
        {"F#1",42},{"G1",43},{"G#1",44},{"A1",45},{"A#1",46},{"B1",47},
        {"C2",48},{"C#2",49},{"D2",50},{"D#2",51},{"E2",52},{"F2",53},
        {"F#2",54},{"G2",55},{"G#2",56},{"A2",57},{"A#2",58},{"B2",59},
        {"C3",60},{"C#3",61},{"D3",62},{"D#3",63},{"E3",64},{"F3",65},
        {"F#3",66},{"G3",67},{"G#3",68},{"A3",69},{"A#3",70},{"B3",71},
        {"C4",72},{"C#4",73},{"D4",74},{"D#4",75},{"E4",76},{"F4",77},
        {"F#4",78},{"G4",79},{"G#4",80},{"A4",81},{"A#4",82},{"B4",83},
        {"C5",84},{"C#5",85},{"D5",86},{"D#5",87},{"E5",88},{"F5",89},
        {"F#5",90},{"G5",91},{"G#5",92},{"A5",93},{"A#5",94},{"B5",95},
        {"C6",96},{"C#6",97},{"D6",98},{"D#6",99},{"E6",100},{"F6",101},
        {"F#6",102},{"G6",103},{"G#6",104},{"A6",105},{"A#6",106},{"B6",107},
        {"C7",108},{"C#7",109},{"D7",110},{"D#7",111},{"E7",112},{"F7",113},
        {"F#7",114},{"G7",115},{"G#7",116},{"A7",117},{"A#7",118},{"B7",119},
        {"C8",120},{"C#8",121},{"D8",122},{"D#8",123},{"E8",124},{"F8",125},
        {"F#8",126},{"G8",127}};
/*
    uint pitchNumber(string& name) {
        return cast<uint>(NoteNumbers[name]);
    }
*/
    array<string> NotesNames = {
        "C-2","C#-2","D-2","D#-2","E-2","F-2","F#-2","G-2","G#-2",
        "A-2","A#-2","B-2","C-1","C#-1","D-1","D#-1","E-1","F-1",
        "F#-1","G-1","G#-1","A-1","A#-1","B-1","C0","C#0","D0",
        "D#0","E0","F0","F#0","G0","G#0","A0","A#0","B0","C1",
        "C#1","D1","D#1","E1","F1","F#1","G1","G#1","A1","A#1",
        "B1","C2","C#2","D2","D#2","E2","F2","F#2","G2","G#2",
        "A2","A#2","B2","C3","C#3","D3","D#3","E3","F3","F#3",
        "G3","G#3","A3","A#3","B3","C4","C#4","D4","D#4","E4",
        "F4","F#4","G4","G#4","A4","A#4","B4","C5","C#5","D5",
        "D#5","E5","F5","F#5","G5","G#5","A5","A#5","B5","C6",
        "C#6","D6","D#6","E6","F6","F#6","G6","G#6","A6","A#6",
        "B6","C7","C#7","D7","D#7","E7","F7","F#7","G7","G#7",
        "A7","A#7","B7","C8","C#8","D8","D#8","E8","F8","F#8","G8"};

    /*
    const string& pitchName(uint pitchNum) {
        return NoteNames[pitchNum];
    }
*/

    array<string> MidiEventNames = {
        "NoteOff",
        "NoteOn",
        "ControlChange",
        "ProgramChange",
        "PitchBend",
        "PolyPressure",
        "ChannelPressure",
        "UndefinedEvent"
    };
/*
    string& MidiEventName(MidiEventType type) {
        return MidiEventNames[type];
    }
*/
    /*****************************************************
     * timestamp conversions
     *****************************************************/

    double secondsFromSamples(double samples) {
        return samples/sampleRate;  // sampleRate is global
    }

    double beatsFromSamples(double samples, double bpm) {
        return (samples/sampleRate) * (bpm/60);  // sampleRate is global
    }

    double ticksFromBeats(double beats, uint ppqn) {
        return floor((fraction(beats) * ppqn)+0.5);
    }

    uint framesFromSeconds(double seconds) {
        // TBD
        return 0;
    }

    uint framesFromSamples(uint64 samples) {
        // TBD
        return 0;
    }


}

   // DP is reportign slightly different values for ticks, sometimes correct
    // and sometimes off by 1 tick above or below what is being calculated below.
    // and I'm not really sure why.  TODO, need to check and verify this math
    // some more to make sure I am not the wrong mis calculating it.  Try in another
    // DAW too.  Verify seconds and samples too.  Try to calculate also Frames
    // which may involve some tricky math to do right, but good to learn that.
    // Even sample time is sometimes right and sometimes off by one cmopared to DP.
    // Try another DAW becuase DP might simply be reporting wrong.  Try with higher
    // PPQN settings also.  It could be rounding something up or down, or I am, 
    // and not sure which is right.
    // TODO, need space for larger PPQN, perhaps 4 digits is enough in reality though
    //       left justify the ticks?
    // TODO try uint64
    //
    // TODO add ability to output a complete proper SMPTE frame, with option of
    //      frames or milliseconds for the sub-second component.
    //      Note that this might involve having to use the GUI to specify the 
    //      starting SMPTE time, since the VST is probably only figuring as 
    //      starting from 0.  But this could be useful for sure to quickly see
    //      how music is hitting certain cue points.  

    // TODO, move some of the calculation functions into library for calculating
    //       between frames, realtime, ticks and samples, which may come in handy
    //       later for other projects with time based processing.


/*
   Notes about local variables

   Should not ever use local variables in functions. Should actually avoid doing too many 
   function calls because angelscript doesn't INLINE.

   But especially make sure all arguments are by reference and don't ever return anything
   other then pointers.  

   For local variables, just use globals in simple cases, or perhaps I should have a 
   local variable factory, one for each type, but looking things up by array element
   is also expensive.  Probably better to just use all globals, but can put them into
   namespaces and use naming conventions to keep them seperated per function

   annoyingly this probably also includes even primitives like i counter, but double 
   check that is really neccessary.  Maybe we only need to do this for objects.  Ask

   this will unfortunatley result in a lot of long variable names

   might need to use a class with each function somehow in order to hold local variables
   which would be re-entrant by nature, but that can be managed.

   I need to do some actual testing to see just how costly it is to use function
   calls with local variables and such.  Maybe for what I do with midi its not 
   worth worrying about and I'm stressing about nothing.

   Sounds like primitive types are not made on the heap for function locals, but all other
   objects like arrays, dictionary, strings, etc.. they are allocated every call,
   so use globals for this always.

   using outputParameters is I guess how to get values to the GUI, so I need to setup 
   a table with outputParameters, probably some KUIML script that basically gets the new
   line as outputparameters and then does a copy shift operation to move all the lines 
   up by one.  something like that.

   PNS author says anything outside of processBlock we don't really need to worry 
   so much about all the mallocing and such becuase on a desktop computer that kind of 
   stuff happens a lot I guess?  I don't know, but I will have to try both ways to make
   sure its no problem to do it easy way versus more complicated ways so that I can 
   let go of my fears about it and just make angelscript easier on myself

 */
