/** \file
 *   MidiMon.as  
 *
 *   AngelScript version of MidiMon, adapted from LogicPro Scripter version
 */

/***
 *
 * TODO
 *      - Improve the GUI, if possible get logging to GUI
 *
 *      - File control to choose the log file to use
 * 
 *      - Able to choose some filtering options
 *
 *      + At a minimum use KUIUML in order to handle
 *        the flushig of buffer outside of ProcessBlock
 *
 *      + Test timing accuracy
 *
 *      - Move some of these into my own hxx library, for example
 *        functions to detect midi types, to get the names of 
 *        events as string, to lookup pitchnames, etc..
 *
 *      - Need to figure out the proper Angelscript way to avoid
 *        mallocing strings and things while calling functions
 *        and especially while building up the logging output
 *        NOTE that this is normally not something needed much by plugins
 *
 *      - Note the right way to handle this is to use KUIML to flush 
 *        a buffer...and have that flushing go to KUIML table as well
 *        as to a console log file.  Need to figure out best way to 
 *        handle the management of a buffer, might have to make a class
 *        to handle it, but still need to figure out how a pre-allocated
 *        buffer would best work and particularly avoiding malloc and 
 *        avoiding string formatting also inside the process block.
 * 
 *      - How to add a GUI for controlling some filtering options.
 *
 *      - How to make a scrolling table view of some kind, well even if
 *        it just displays that last 10-20 rows with no scroll bar, would 
 *        be useful.
 *
 *      - As a test case, make this in BIN form and try to compile as native
 *        also to see how its done.  
 *
 *      - As a test also try to replace the complete SKIN with my own so
 *        that the plugin is foolproof and looks proprietary.
 *
 *      - For my own library, need some angelscript functions to simplify
 *        life, like sprintf maybe...but how often do I really need that
 *        in reality beyong MidiMon?  Anyway, I'm also very unhappy with the
 *        local variable and function parameter situation, need a system 
 *        that can use some global stash of variables in some way...to make 
 *        up for Angelscript's half assed stack.
 *
 *      - Need to develop some PNS design patterns for using KUIML
 *        and probably need to figure out how to use images to make
 *        cool looking plugins that look better then the default.
 *
 *      - Possible to display SMPTE frames? probably yes but GUI has to provide frame rate
 *
 ***/


//#include "../library/Midi.hxx"
#include "./myLibrary/MyMidi.hxx"

// filter interface-------------------------
string name        = "MidiMon";
string description = "Logs incoming MIDI events";

uint PPQN           = 960;



/**************************************************
 *********** DO NOT EDIT BELOW HERE
 **************************************************/


/******************************************
 * processBlock
 ******************************************/

void processBlock(BlockData& data)
{
    for(uint i=0;i<data.inputMidiEvents.length;i++)
    {
        const MidiEvent@ evt = data.inputMidiEvents[i];

        // print event
        logEvent(evt, data);

        // forward event (unchanged)
        data.outputMidiEvents.push(evt);
    }
}

string consoleMessage = "";

/******************************************
 * logEvent
 *
 * TODO, think through all this to make sure
 *       strings are handling as efficient as 
 *       possible to avoid malloc
 ******************************************/

void logEvent(const MidiEvent& evt, BlockData& data)
{

    consoleMessage = "";

    consoleTimingStr(evt, data);

    MidiEventType type = MidiEventUtils::getType(evt);
    consoleMessage += MyMidi::MidiEventNames[type];

    switch(type)
    {
    case kMidiNoteOn:
    case kMidiNoteOff:
        {
            consoleMessage+=" " + MidiEventUtils::getNote(evt);
            consoleMessage+=" Vel: " + MidiEventUtils::getNoteVelocity(evt);
            break;
        }
    case kMidiControlChange:
        {
            consoleMessage+=" " + MidiEventUtils::getCCNumber(evt);
            consoleMessage+=" Val: " + MidiEventUtils::getCCValue(evt);
            break;
        }
    case kMidiProgramChange:
        {
            consoleMessage+=" " + MidiEventUtils::getProgram(evt);
            break;
        }
    case kMidiPitchWheel:
        {
            consoleMessage+=" " + MidiEventUtils::getPitchWheelValue(evt);
            break;
        }
    case kMidiChannelAfterTouch:
        {
            consoleMessage+=" Value: " + MidiEventUtils::getChannelAfterTouchValue(evt);
            break;
        }
    case kMidiNoteAfterTouch:
        {
            consoleMessage+=" " + MidiEventUtils::getNote(evt);
            consoleMessage+=" Value: " + MidiEventUtils::getNoteVelocity(evt);
            break;
        }
    }
    consoleMessage+=" on Ch.";
    consoleMessage+=MidiEventUtils::getChannel(evt);;
    consoleMessage+=" (";
    consoleMessage+=evt.byte0;
    consoleMessage+="/";
    consoleMessage+=evt.byte1;
    consoleMessage+="/";
    consoleMessage+=evt.byte2;
    consoleMessage+=")";
    print(consoleMessage);
}

/* TODO, probably pass in allocated string by reference to improve performance */
/* TODO, handle various different timestamp modes, by fraction, ms, sample, ticks */
void consoleTimingStr(const MidiEvent& evt, BlockData& data)
{
     double totalSamples = data.transport.get_positionInSamples() + floor(evt.timeStamp+0.5);

     double totalSeconds = MyMidi::secondsFromSamples(totalSamples);

     // Should we do this or use block start beat from PNS?
     double totalBeats = MyMidi::beatsFromSamples(totalSamples, data.transport.get_bpm());

     double beat = floor(totalBeats);
     double ticks = MyMidi::ticksFromBeats(totalBeats, PPQN);

     // TODO use the PNS format functions that avoid malloc
     consoleMessage += "[" + formatFloat(beat,"",4) + "|" + formatFloat(ticks,"",4) 
         + "|" + formatFloat(totalSeconds,"",8,3) + "|" + formatFloat(totalSamples, "", 9) + "] ";
}


